/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 26, 2015, 11:23 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string name;
    cout << "Hello, my name is HAL!"<< endl ;
    cout << "What is your name?" << endl;
    cin >> name;
    cout << "Hello, "<< name <<" I am glad to meet you!" << endl;
    return 0;
}

